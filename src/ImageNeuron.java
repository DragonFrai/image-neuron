import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;

public class ImageNeuron extends JFrame {

    private PaintPanel paint;
    private NeuronNetwork network;
    private NetworkPanel networkPanel;
    private JLabel answerLabel;
    private String[] variants;
    // ComboBox С вариантами ответов
    private JComboBox answerCBox;

    public ImageNeuron(Dimension imageSize, String[] variants) {
        this(imageSize, variants, new int[] {});
    }

    ImageNeuron(Dimension imageSize, String[] variants, int[] hidenLayers) {
        super("Neuron");

        this.variants = variants;

        // ### ПАНЕЛЬ ДЛЯ РИСОВАНИЯ
        // Создаем панель для рисования
        paint = new PaintPanel(imageSize.width, imageSize.height);
        paint.setBorder(BorderFactory.createLineBorder(Color.black));

        // Кнопка очистки с панели
        JButton clearBtn = new JButton("Очистить");
        clearBtn.addActionListener(actionEvent -> paint.clear());

        // Кнопка "Узнать у сети"
        JButton whatBtn = new JButton("Что это?");
        whatBtn.addActionListener(event -> whatIsIt());

        answerCBox = new JComboBox();
        for (String item: variants) {
            answerCBox.addItem(item);
        }

        JButton learn = new JButton("Обучить");
        learn.addActionListener(e -> {
            String currect = answerCBox.getSelectedItem().toString();
            int index = 0;
            for (int i = 0; i < variants.length; i++) {
                if (variants[i].equals(currect)) {
                    index = i;
                }
            }
            Layer out = network.layers[network.layers.length - 1];
            double[] answer = new double[out.outputs()];
            answer[index] = 1.0;
            network.learn(answer);
            whatIsIt();
        });

        // Собираем все вместе
        JPanel controlPanel = new JPanel();
        controlPanel.setAlignmentY(0.5f);
        paint.setAlignmentX(Component.CENTER_ALIGNMENT);
        clearBtn.setAlignmentX(Component.CENTER_ALIGNMENT);
        whatBtn.setAlignmentX(Component.CENTER_ALIGNMENT);
        answerCBox.setAlignmentX(Component.CENTER_ALIGNMENT);
        learn.setAlignmentX(Component.CENTER_ALIGNMENT);

        controlPanel.setLayout(new BoxLayout(controlPanel, BoxLayout.PAGE_AXIS));

        controlPanel.add(paint);
        controlPanel.add(clearBtn);
        controlPanel.add(whatBtn);
        controlPanel.add(answerCBox);
        controlPanel.add(learn);


        /// ### Панель отображения нейронов и нейронная сеть
        // Количество нейронов в слоях нейронной сети
        int layersCount = hidenLayers.length + 2;
        int[] layers = new int[layersCount];
        layers[0] = imageSize.width * imageSize.height;
        layers[layers.length - 1] = variants.length;
        System.arraycopy(hidenLayers, 0, layers, 1, hidenLayers.length);

        // Инициализация сети и панели её отображающей
        network = new NeuronNetwork(layers, ActivationFunction.SOFT_SIGN);
        networkPanel = new NetworkPanel(network);

        JScrollPane scrollPane = new JScrollPane(networkPanel);
        //scrollPane.setLayout(new ScrollPaneLayout());
        scrollPane.createVerticalScrollBar();
        //scrollPane.add(networkPanel);

        // Label для коммуникации с пользователем
        answerLabel = new JLabel();
        answerLabel.setMinimumSize(new Dimension(100, 20));

        // ### ОСНОВНАЯ ПАНЕЛЬ
        JPanel mainPanel = new JPanel();
        BorderLayout mainLayout = new BorderLayout(0, 5);
        mainPanel.setLayout(mainLayout);

        JPanel westPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 5, 5));
        westPanel.add(controlPanel);
        mainPanel.add(westPanel, BorderLayout.WEST);
        mainPanel.add(scrollPane, BorderLayout.CENTER);
        mainPanel.add(answerLabel, BorderLayout.SOUTH);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setContentPane(mainPanel);
        pack();

        // Размер экрана
        Dimension size = getBounds().getSize();
        // Если размер окна больше 80% всего окна -- уменьшаем его
        int hLimit = (int) (size.height * 0.8);
        if (this.getHeight() > hLimit) {
            this.setSize(getWidth(), hLimit);
        }
    }

    // Метод, который решает что на картинке
    public void whatIsIt() {
        BufferedImage im = paint.getImage();
        double[] inputs = Utils.imageToVector(im);
        network.setInputs(inputs);
        networkPanel.repaint();
        Layer out = network.layers[network.layers.length - 1];
        double max = Double.NEGATIVE_INFINITY;
        int variant = -1;
        for (int i = 0; i < out.outputs(); i++) {
            if (out.getValue(i) > max) {
                max = out.getValue(i);
                variant = i;
            }
        }
        answerLabel.setText("Нейросеть думает, что правильный ответ это " + variants[variant] + ".");
        networkPanel.repaint();
    }

    public static void main(String[] args) {

        ImageNeuron frame = new ImageNeuron(
                new Dimension(8, 8), // Размер картинки
                new String[] {"0", "1", "2"}, // Варианты ответа
                new int[] {16, 32, 4, 16} // Кол-во нейронов в промежуточных слоях
                );

        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

    }
}


class NeuronDialog extends JFrame {
    NeuronDialog() {

    }
}



class Utils {

    // Функция, которая делает из изображение матрицу для нейросети
    public static double[] imageToVector(BufferedImage im) {
        double[] data = new double[im.getWidth() * im.getHeight()];
        for (int x = 0; x < im.getWidth(); x++) {
            for (int y = 0; y < im.getHeight(); y++) {
                double[] c = new double[3];
                im.getData().getPixel(x, y, c);
                double signal = 1.0;
                signal -= c[0] / (3.0*255.0);
                signal -= c[1] / (3.0*255.0);
                signal -= c[2] / (3.0*255.0);
                data[y * im.getWidth() + x] = signal;
            }
        }
        return data;
    }

}

























// Панель, на которой мы можем рисовать
class PaintPanel extends JPanel {

    BufferedImage image;

    // Это масштаб картинки для удобства (Чтобы картинка 16 на 16 не была 16 на 16 пикселей)
    int scale = 16;

    public PaintPanel(int width, int height) {
        image = new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR);
        clear();
        // Устанавливаем размер панели
        Dimension size = new Dimension(width*scale, height*scale);
        this.setMinimumSize(size);
        this.setPreferredSize(size);
        this.setMaximumSize(size);

        addMouseMotionListener(
                new MouseMotionListener() {

                    // Рисуем на панели
                    @Override
                    public void mouseDragged(MouseEvent mouseEvent) {
                        // Так как картинака отображается увеличенной, переводим координаты панели в координаты картинки
                        int imageX = mouseEvent.getX() / scale;
                        int imageY = mouseEvent.getY() / scale;

                        // Закрашиваем точку
                        Graphics g = image.getGraphics();
                        g.setColor(Color.black);
                        g.drawRect(imageX, imageY, 0, 0);
                        repaint();
                    }

                    @Override
                    public void mouseMoved(MouseEvent event) {}
                }
        );
    }

    // Просто геттер картинки
    public BufferedImage getImage() {
        return image;
    }

    // Очищает изображение белым
    public void clear() {
        Graphics g = image.getGraphics();
        g.setColor(Color.white);
        g.fillRect(0, 0, image.getWidth(null), image.getHeight(null));
        repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(image, 0, 0, image.getWidth(null)*scale, image.getHeight(null)*scale, null);
    }
}


