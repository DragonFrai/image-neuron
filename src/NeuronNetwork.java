





public class NeuronNetwork {

    // Функция активации
    ActivationFunction function;

    // Коэфициент обучения
    double learnSpeed = 0.1;

    Layer[] layers;

    public NeuronNetwork(int[] neurons, ActivationFunction function) {

        this.function = function;

        layers = new Layer[neurons.length];
        // Создаем входной слой
        layers[0] = new Layer(neurons[0]);
        for (int i = 1; i < neurons.length; i++) {
            int outputs = neurons[i];
            int inputs = neurons[i - 1];
            this.layers[i] = new Layer(outputs, inputs, function);
        }
    }

    // Устанавливает входные значения нейроном и обновляет промежуточные результаты
    public void setInputs(double[] values) {
        layers[0].setValues(values);
        for (int layer = 1; layer < layers.length; layer++) {
            layers[layer].setInputs(layers[layer-1]);
        }
    }

    public void learn(double[] desired) {
        Layer learned = layers[layers.length - 1];
        double[] errors = new double[learned.outputs()];
        // Цикл по нейронам слоя
        for (int neuron = 0; neuron < learned.outputs(); neuron++) {
            double error = desired[neuron] - learned.getValue(neuron);
            errors[neuron] = error;
        }
        learnLayer(layers.length - 1, errors);
    }

    private void learnLayer(int layer, double[] errors) {
        if (layer == 0) {
            throw new IllegalArgumentException("Нельзя обучить входной слой.");
        }
        if (layer < 0 || layer >= layers.length) {
            throw new IllegalArgumentException("Слоя с индексом " + layer + "не существует.");
        }
        // обучаемый слой
        Layer learned = layers[layer];
        if (learned.outputs() != errors.length) {
            throw new IllegalArgumentException("Размер слоя не совпадает с размером ожидаемого ошибки.");
        }


        //Ошибки предыдущего слоя
        double[] prevErrors = new double[learned.inputs()];

        // Цикл по нейронам слоя
        for (int neuron = 0; neuron < learned.outputs(); neuron++) {
            double error = errors[neuron];
            double value = learned.getValue(neuron);
            double delta = error * learned.getActivationFunction().derivative(value);

            // Проходимся по каждому весу каждого нейрона и корректируем его
            for (int input = 0; input < learned.inputs(); input++) {
                prevErrors[input] += learned.getWeight(neuron, input) * delta;
                // Значение на текущем нейроне
                double lastValue = layers[layer - 1].getValue(input);
                double d = delta * lastValue * learnSpeed;
                learned.setWeight(neuron, input, learned.getWeight(neuron, input) + d);
            }
        }

        if (layer > 1) {
            learnLayer(layer - 1, prevErrors);
        }
    }

    public Layer getOutput() {
        return layers[layers.length - 1];
    }

}