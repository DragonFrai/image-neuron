


// Матрица double
public class Matrix implements Cloneable{
    // кол-во строк матрицы
    int lines;
    // кол-во столбцов
    int rows;
    // Массив с данными матрицы
    double[][] data;

    // Инициализация пустой матрицы с данными размерами
    public Matrix(int lines, int rows) {
        this.lines = lines;
        this.rows = rows;
        this.data = new double[lines][rows];
    }

    // Создаем матрицу из вектора
    public Matrix(double[] vec) {
        this(vec.length, 1);
        for(int line = 0; line < vec.length; line++) {
            set(line, 0, vec[line]);
        }
    }

    // Возвращает число матрицы в данной позиции
    public double get(int line, int row) {
        return data[line][row];
    }

    public double get(int el) {
        return get(el, 0);
    }

    // устанавиливает число в данную позицию
    public void set(int line, int row, double value) {
        data[line][row] = value;
    }

    // Метод, который переменожает матрицы
    public Matrix multiply(Matrix other) {
        if (this.rows != other.lines) {
            throw new IllegalArgumentException("Эти матрицы не могут быть перемножены");
        }

        Matrix newM = new Matrix(this.lines, other.rows);

        for (int line = 0; line < this.lines; line++) {
            for (int newRow = 0; newRow < other.rows; newRow++) {
                double newEl = 0.0;
                for (int row = 0; row < this.rows; row++) {
                    newEl += this.get(line, row) * other.get(row, newRow);
                }
                newM.set(line, newRow, newEl);
            }
        }

        return newM;
    }

    public Matrix clone() {
        Matrix m = new Matrix(lines, rows);
        for (int line = 0; line < lines; line ++) {
            for (int row = 0; row < rows; row ++) {
                m.set(line, row, this.get(line, row));
            }
        }
        return m;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        for (double[] line : data) {
            s.append("[");
            for (double el : line) {
                s.append(el);
                s.append("; ");
            }
            s.append("]\n");
        }
        return s.toString();
    }
}