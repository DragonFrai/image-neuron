import javax.swing.plaf.synth.SynthLookAndFeel;
import java.util.Random;

public class Layer {

    // Кол-во выходов (нейронов) в слое
    private int outputs;
    // Кол-во входов (нейронов на предыдущем слое)
    // Если входов 0 -- слой считается входным
    private int inputs;

    // Значение выходных нейронов
    private double[] values;

    // Веса слоя [нейронов этого слоя][нейронов предыдущего слоя]
    private double[][] weights;

    // Функция активации в этом слое
    private ActivationFunction activationFunction;


    public Layer(int outputs, int inputs, ActivationFunction func) {
        this.inputs = inputs;
        this.outputs = outputs;
        values = new double[outputs];
        weights = new double[outputs][inputs];

        this.activationFunction = func;

        // Задаем веса
        Random rng = new Random();
        for (int neuron = 0; neuron < outputs(); neuron++) {
            for (int input = 0; input < inputs(); input++) {
                setWeight(neuron, input, rng.nextDouble() - 0.5);
            }
        }
    }

    public Layer(int outputs) {
        this(outputs, 0, null);
    }

    // Высчитывает выходы слоя из входов
    public void setInputs(Layer inputs) {
        if (inputs.outputs() != this.inputs) {
            throw new IllegalArgumentException("Кол-во входов не совпадает с ожидаемым.");
        }
        // Для каждого нейрона вычисляем сумму входов нв веса.
        for (int neuron = 0; neuron < values.length; neuron++) {
            double sum = 0;
            for (int input = 0; input < inputs.outputs(); input++) {
                // Получаем нужный нам вес связи.
                double weigh = getWeight(neuron, input);
                // Увеличиваем сумму
                sum += weigh * inputs.getValue(input);
            }
            // Применяем функцию активации к нашей сумме
            double value = activationFunction.activate(sum);
            values[neuron] = value;
        }
    }

    public ActivationFunction getActivationFunction() {
        return activationFunction;
    }

//    // Обучает слой, основываясь на ошибке. Возвращает ошибки на предыдущем слое
//    public double[] learn(double[] errors) {
//        //Ошибки предыдущего слоя
//        double[] prevErrors = new double[inputs];
//
//        // Цикл по нейронам слоя
//        for (int neuron = 0; neuron < outputs(); neuron++) {
//            double error = errors[neuron];
//            double value = getValue(neuron);
//            double delta = error * activationFunction.derivative(value);
//
//            // Проходимся по каждому весу каждого нейрона и корректируем его
//            for (int input = 0; input < inputs(); input++) {
//                prevErrors[input] += getWeight(neuron, input) * delta;
//                // Значение на текущем нейроне
//                double lastValue = layers[layer - 1].get(input);
//                double d = delta * lastValue * learnSpeed;
//                weights.set(neuron, input, weights.get(neuron, input) + d);
//            }
//        }
//
//        return prevErrors;
//    }

    // Кол-во выходов слоя (== кол-ву нейронов)
    public int outputs() {
        return outputs;
    }

    // Ожидаемое кол-во нейронов во входном слое
    public int inputs() {
        return inputs;
    }

    // Возвращает значение нейрона
    public double getValue(int neuron) {
        return values[neuron];
    }

    public void setValues(double[] values) {
        if (inputs != 0) {
            throw new IllegalStateException("Нельзя установить значения в не входном слое");
        }
        if (values.length != this.values.length) {
            throw new IllegalArgumentException("Кол-во нейронов не совпадает с кол-вом элементов массива");
        }
        System.arraycopy(values, 0, this.values, 0, values.length);
    }

    // neuron -- нейрон текущего слоя, чьи веса мы получаем
    // toNeuron -- нейрон,вес связи с которым мы получаем
    public double getWeight(int neuron, int toNeuron) {
        return weights[neuron][toNeuron];
    }

    // neuron -- нейрон текущего слоя, чьи веса мы получаем
    // toNeuron -- нейрон,вес связи с которым мы получаем
    public void setWeight(int neuron, int toNeuron, double weight) {
        weights[neuron][toNeuron] = weight;
    }

}
