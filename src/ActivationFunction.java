// Интерфейс функции активации
public interface ActivationFunction {

    ActivationFunction SOFT_SIGN =
            new ActivationFunction() {
                @Override
                public double activate(double val) {
                    return val / (1.0 + Math.abs(val));
                }

                @Override
                public double derivative(double val) {
                    return 1.0 / (1.0 + Math.abs(val)) - val / Math.pow(1.0 + Math.abs(val), 2.0);
                }
            };

    double activate(double val);
    double derivative(double val);
}