import javax.swing.*;
import java.awt.*;

public class NetworkPanel extends JPanel {

    private NeuronNetwork network;

    // Константы для рисования

    // Толщина отступа
    private static final int BORDER = 24;
    // Размер нейрона
    private static final int SIZE = 12;

    // Расстояние между слоями
    private static final int SPACING = 100;

    private static final Color M_ONE_COLOR = Color.red; // -1
    private static final Color ZERO_COLOR = new Color(255, 255, 0, 0); // 0
    private static final Color ONE_COLOR = Color.green; // 1

    public NetworkPanel(NeuronNetwork network) {
        this.network = network;

        int height = 0;
        for (int i = 0; i < network.layers.length; i++) {
            int h = network.layers[i].outputs() * (SIZE + 1) + BORDER*2;
            if (h > height) { height = h; }
        }

        int width = SPACING * network.layers.length + BORDER*2;

        Dimension size = new Dimension(width, height);
        setMaximumSize(size);
        setMinimumSize(size);
        setSize(size);
        setPreferredSize(size);
    }

    private int getLayerXPos(int layer) {
        return BORDER + SPACING * layer;
    }

    private Point getNeuronPos(int layer, int neuron) {
        int x = SPACING * layer;
        int h = getHeight() - 2*BORDER;
        int ih = network.layers[layer].outputs() * (SIZE + 1);
        int yOffset = (h - ih) / 2;
        int y = (SIZE + 1) * neuron;
        return new Point(x, y+yOffset);
    }

    private static Color blendColors(Color c1, Color c2, double n) {
        double n1 = 1.0 - n;
        double n2 = n;
        int r = (int) (n1 * c1.getRed() + n2 * c2.getRed());
        int g = (int) (n1 * c1.getGreen() + n2 * c2.getGreen());
        int b = (int) (n1 * c1.getBlue() + n2 * c2.getBlue());
        int a = (int) (n1 * c1.getAlpha() + n2 * c2.getAlpha());
        return new Color(r, g, b, a);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        // Делаем отступ от края
        g.translate(BORDER, BORDER);

        // Рисуем веса
        // Цикл по весам и соответствующим слоям
        for (int i = 1; i < network.layers.length; i++) {
            Layer layer = network.layers[i];
            Layer nextLayer = network.layers[i - 1];
            // Цикл по каждому нейрону текущего слоя
            for (int neuron = 0; neuron < layer.outputs(); neuron++) {
                double value = layer.getValue(neuron);
                // Цикл по весам следующего слоя
                for (int nextNeuron = 0; nextNeuron < nextLayer.outputs(); nextNeuron++) {
                    // Получаем вес, соответствующий текущему нейрону
                    double weight = layer.getWeight(neuron, nextNeuron);
                    double w = weight * value;
                    if (w < -1.0) w = -1.0;
                    if (w > 1.0) w = 1.0;
                    Color c;
                    if (w >= 0.0) {
                        c = blendColors(ZERO_COLOR, ONE_COLOR, w);
                    } else {
                        c = blendColors(M_ONE_COLOR, ZERO_COLOR, w + 1.0);
                    }

                    // Точка нейрона текущег слоя
                    Point p1 = getNeuronPos(i, neuron);
                    Point p2 = getNeuronPos(i-1, nextNeuron);

                    g.setColor(c);
                    g.drawLine(p1.x, p1.y, p2.x, p2.y);
                }
            }
        }

        // Цикл рисования нейронов
        for (int i = 0; i < network.layers.length; i++) {
            Layer layer = network.layers[i];
            // Цикл по каждому нейрону текущего слоя
            for (int neuron = 0; neuron < layer.outputs(); neuron++) {
                double value = layer.getValue(neuron);
                if (value < -1.0) value = -1.0;
                if (value > 1.0) value = 1.0;
                Color c;
                if (value >= 0.0) {
                    c = blendColors(ZERO_COLOR, ONE_COLOR, value);
                } else {
                    c = blendColors(M_ONE_COLOR, ZERO_COLOR, value + 1.0);
                }
                c = new Color(c.getRGB());
                Point p = getNeuronPos(i, neuron);

                g.setColor(c);
                g.fillOval(p.x - SIZE/2 + 1, p.y - SIZE/2 + 1, SIZE - 1, SIZE - 1);
                g.setColor(Color.black);
                g.drawOval(p.x - SIZE/2 + 1, p.y - SIZE/2 + 1, SIZE - 1, SIZE - 1);
            }
        }
    }
}